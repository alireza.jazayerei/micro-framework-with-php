<?php
include "Core/Autoloader.php";
include "Core/Definer.php";

session_start();

use Dispatch\Dispatch;
use Dispatch\Request;

$request = new Request();
$dispatch = new Dispatch($request);
$dispatch->dispatcher();

