<?php

namespace Dispatch;

class Request
{
    public $controller = "Home";
    public $method = "index";
    public $params = array();
    private $url;

    //every new request comes here
    public function __construct()
    {
        $this->url = strtolower($_GET["url"]);
    }


    public function getUrl()
    {
        return $this->url;
    }
}