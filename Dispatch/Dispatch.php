<?php

namespace Dispatch;

use Core\Helpers\Helper;
use Core\Interfaces\AppLoader;

class Dispatch
{
    private $url;
    private $request;

    public function __construct(Request $request)
    {
        $this->url = trim($request->getUrl(), "/"); //delete the / character from first and end of the requested url
        $this->request = $request;
    }

    public static function middlewareCaller($middleware)
    {
        if (strpos($middleware, ",") !== false) {
            return self::multiMiddleware($middleware);
        }

        if (strpos($middleware, "-") !== false) {
            return self::middlewareHasParam($middleware);
        }
        $middlewareClass = "Core\Middlewares\\" . ucfirst($middleware) . "Middleware";
        $middleware = new $middlewareClass;
        if (method_exists($middleware, 'handler')) {
            return $middleware->handler();
        }
        exit;
    }

    private static function multiMiddleware($middleware)
    {
        $middlewares = explode(",", $middleware);
        $response = [];
        foreach ($middlewares as $middleware) {
            if (strpos($middleware, "-") !== false) {
                $response[] = self::middlewareHasParam($middleware);
            }
            else {
                $middlewareClass = "Core\Middlewares\\" . ucfirst($middleware) . "Middleware";
                $middleware = new $middlewareClass;
                if (method_exists($middleware, 'handler')) {
                    $response[] = $middleware->handler();
                }
            }
        }
        $status = in_array(false, $response);
        if ($status) {
            return false;
        }
        return true;
    }

    private static function middlewareHasParam($middleware)
    {
        [$middleware, $param] = explode("-", $middleware);
        $middlewareClass = "Core\Middlewares\\" . ucfirst($middleware) . "Middleware";
        $middleware = new $middlewareClass;
        if (method_exists($middleware, 'handler')) {
            return $middleware->handler($param);
        }
        exit;
    }

    public function dispatcher() //get the request and call the route for it and then load controller
    {
        //call the route [it sets the controller and etc....]
        Router::render($this->request, $this->url);

        //load controller [create an object of it]
        $controller = $this->loadController();
        $method = $this->request->method;
        //todo make params happen like /hollo/{id}
        $params = $this->request->params;

        call_user_func_array([$controller, $method], $params);
    }

    private function loadController()
    {
        $namespace = "\App\Controllers\\";
        $controllerName = $namespace . $this->request->controller . "Controller";
        return new $controllerName;
    }
}