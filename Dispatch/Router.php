<?php

namespace Dispatch;

use Core\Helpers\Helper;
use Core\Middlewares\Middleware;

class Router
{
    //use this when you are sending data in get method or you dont send data at all [like a simple link]
    public static function render(Request $request, $url)
    {
        $route = Router::exists($url);

        if ($route !== false) {

            $middleware = $route["middleware"] ?? NULL;
            //if there is any middleware check it

            if (($middleware === NULL || Dispatch::middlewareCaller($middleware)) && $_SERVER["REQUEST_METHOD"] === strtoupper($route["type"])) { //if the http method is GET

                $request->controller = $route["controller"];
                $request->method = $route["method"];
                //todo make params happen like /helloe/{id}
                //$request->params = array_slice($url, "1");
                return;
            }
        }
        Helper::redirect("404");
    }

    public static function exists($url)
    {
        include "Web.php";

        if (array_key_exists($url, $routes)) //if the requested url matches any route
        {
            return $routes[$url];
        }
        return false;
    }

}