<?php


namespace App\Models;

use Core\Helpers\Helper;
use Core\Model;

class User extends Model
{
    private $table = "users";

    public static function check()
    {
        if (isset($_SESSION["id"])) {
            return true;
        }
        return false;
    }

    public static function userId()
    {
        if (isset($_SESSION["id"])) {
            return $_SESSION["id"];
        }
        return false;
    }

    public function checkCredentials($data) //we did the works with data here in the model
    {
        $result = $this->read($this->table, "users.id", "users.username='$data[username]' AND users.password='$data[password]'");
        if (count($result) === 1) {
            $result = reset($result); //get the array inside array result
            return $result["id"];
        }
        return false;
    }

    public function setRememberToken($signature)
    {
        $this->update($this->table, array("signature"), array($signature), "id='$_SESSION[id]'");
    }

    public function removeRememberToken()
    {
        $this->update($this->table, array("signature"), array("NULL"), "id='$_SESSION[id]'");
    }

    public function signUp($data)
    {
        $this->create($this->table, array("name", "username", "password"), $data);
    }

    public function getPermissions()
    {
        return $this->read("permission_has_user", "permission_id", "user_id=$_SESSION[id]");
    }
}