<?php


namespace App\Controllers;


use Core\Controller;

class ErrorController extends Controller
{
    public function notFound()
    {
        $this->view("404View");
    }
}