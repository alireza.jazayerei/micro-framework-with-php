<?php


namespace App\Controllers;


use App\Models\User;
use Core\Config\Validation;
use Core\Controller;
use Core\Helpers\Helper;
use Core\Traits\ErrorHandler;


class UserController extends Controller
{
    use ErrorHandler;

    private $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function loginIndex() //shows the login form
    {
        $this->view("LoginView");
    }

    public function signUpIndex()
    {
        $this->view("SignUpView");
    }

    public function doLogin() //todo ask err problem route login/dologin
    {
        $inputData = $_POST["frm"];

        $validator = Validation::make([
            "password" => "min-8,upper,lower,number,required",
            "username" => "min-4,max-20,required",
        ], $inputData);

        if ($validator->failed()) {
            $errors = $validator->getErrors();
            $data = array(
                "errors" => $errors,
            );
            $this->view("LoginView", $data);
            return;
        }
        //hash the password
        $inputData["password"] = md5($inputData["password"]);

        if ($this->user->checkCredentials($inputData) !== false) { //check user/pass with DB
            $_SESSION["id"] = $this->user->checkCredentials($inputData);
            if (isset($inputData["remember"])) {
                $signature = Helper::getRandomString();
                $this->user->setRememberToken($signature); //update DB with cookie signature
                //set cookie to remember user
                setcookie("remember", $signature, "86400");
            }

            //get the users permissions
            $permissions = $this->user->getPermissions();
            $_SESSION["permissions"] = array_column($permissions, "permission_id");

            Helper::redirect("dashboard"); //redirects to a given route
            return;
        }

        //if user/pass was wrong
        $this->addError("form", "Wrong Username or Password");
        $errors = $this->getErrors();
        $data = array(
            "errors" => $errors,
        );
        $this->view("LoginView", $data);
    }

    public function doSignUp()
    {
        $inputData = $_POST["frm"];

        $validator = Validation::make([
            "password" => "min-8,upper,lower,number,required",
            "username" => "min-4,max-20,required",
            "name" => "required,alphaNumeric"
        ], $inputData);

        if ($validator->failed()) {
            $errors = $validator->getErrors();
            $data = array(
                "errors" => $errors,
            );
            $this->view("SignUpView", $data);
            return;
        }

        $inputData["password"] = md5($inputData["password"]);
        $this->user->signUp($inputData);

        Helper::redirect("login");
    }

    public function logOut()
    {
        $this->user->removeRememberToken();
        setcookie("remember", "", "-86400");
        session_destroy();
        Helper::redirect("");
    }

}