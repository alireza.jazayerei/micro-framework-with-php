<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
</head>
<body>
<h1>Dashboard</h1>
<a href="<?= ACTION ?>Logout">LOGOUT</a>
<br><br>
<?php if ($this->can(EDIT_USERS)): ?>
    <a href="<?= ACTION ?>Dashboard/EditUsers">EDIT USERS</a>
    <br>
<?php endif;
if ($this->can(EDIT_USERS)): ?>
    <a href="<?= ACTION ?>Dashboard/EditUsers">DELETE USERS</a>
<?php endif; ?>
</body>
</html>