<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
</head>
<body>
<h1>Hi This is Home page</h1>
<?php use App\Models\User;

if (!User::check()): ?>
    <a href="<?= ACTION ?>Login">LOGIN</a>
    <a href="<?= ACTION ?>SignUp">SignUp</a>
<?php endif;
if (User::check()): //check if user has logged in?>
    <a href="<?= ACTION ?>Dashboard">Dashboard</a>
<?php endif; ?>
</body>
</html>