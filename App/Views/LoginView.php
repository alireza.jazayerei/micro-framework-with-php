<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>

<form action="<?php ACTION ?>Login/DoLogin" method="post">
    <?= @$errors["form"] . "<br>"; //printing errors for each field?>
    <input type="text" name="frm[username]">
    <br>
    <?php
    if (isset($errors["username"])):
        foreach ($errors["username"] as $error) {
            echo $error . "<br>";
        } endif; ?>
    <input type="password" name="frm[password]">
    <br>
    <?php if (isset($errors["password"])): foreach (@$errors["password"] as $error) {
        echo $error . "<br>";
    } endif; ?>
    <input type="submit">
</form>

</body>
</html>