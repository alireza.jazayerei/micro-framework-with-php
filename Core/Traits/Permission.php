<?php


namespace Core\Traits;


trait Permission
{
    private function can($permissionId)
    {
        if (in_array($permissionId, $_SESSION["permissions"])) {
            return true;
        }
        return false;
    }
}