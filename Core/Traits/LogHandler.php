<?php


namespace Core\Traits;


trait LogHandler
{
    private function collectUserLog($userIdentifier,$requestedPath)
    {

        //set time zone to tehran
        date_default_timezone_set("Asia/tehran");
        $time = date("H:i:s");
        $todayDate = date("Y-m-d");

        //check if today's directory exists or make it
        if (!is_dir("Files/Logs/" . $todayDate)) {
            mkdir("Files/Logs/" . $todayDate);
        }

        //this is for local Ip address
        $userIdentifier=str_replace("::","",$userIdentifier);

        //the logs file address to open or create
        $fileDirectory = LOG_PATH . "/" . $todayDate ."/". $userIdentifier . '.txt';
        //open in read and write mode [we need both]
        $file = fopen($fileDirectory, 'a+');

        $line = "IP/USER ID : " . $userIdentifier . " || PAGE: " . $requestedPath . " || TIME : " . $time . "\r\n";
        fwrite($file, $line);

        fclose($file);
    }
}