<?php


namespace Core\Traits;


trait ErrorHandler
{
    private static $errors = array(); //stores all of errors
    private static $fieldErrors = array(); //just error for 1 field

    public static function addFieldError($error)
    {
        self::$fieldErrors[] = $error;
    }

    public static function addError($field, $errors)
    {
        self::$errors[$field] = $errors;
        self::$fieldErrors = array();
    }

    public function getErrors() //if there is not any errors return true
    {
        if (self::hasError()) {
            return self::$errors;
        }
        exit();
    }

    public static function hasError()
    {
        self::$errors = array_filter(self::$errors);
        if (empty(self::$errors)) {
            return false;
        }
        return true;
    }

    public function getFieldErrors($field) //return a specific field error from errors array
    {
        if (array_key_exists($field, self::$errors)) {
            return self::$errors[$field];
        }
        return false;
    }
}