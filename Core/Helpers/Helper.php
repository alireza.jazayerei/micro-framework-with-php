<?php

namespace Core\Helpers;

class Helper
{
    //helper functions are here
    //all of them are static
    public static function getRandomString($length = 32)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        return $string;
    }
    //redirects to a route
    public static function redirect($route)
    {
        header("location:" . ACTION . $route);
        exit();
    }
}