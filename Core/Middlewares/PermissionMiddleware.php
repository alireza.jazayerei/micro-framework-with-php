<?php


namespace Core\Middlewares;


use Core\Interfaces\Middleware;
use Core\Traits\Permission;

class PermissionMiddleware implements Middleware
{

    use Permission;

    public function handler($permissionId = NULL)
    {
        if ($permissionId !== NULL) {
            return $this->can($permissionId);
        }
        exit;
    }
}