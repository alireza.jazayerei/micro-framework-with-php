<?php

namespace Core\Middlewares;

use App\Models\User;
use Core\Helpers\Helper;

class AuthMiddleware implements \Core\Interfaces\Middleware
{

    public function handler()
    {
        if (User::check()) {
            return true;
        }
        Helper::redirect("login");
        exit;
    }
}