<?php

namespace Core\Config;

use Core\Traits\ErrorHandler;

class Validation
{
    use ErrorHandler;
    private static $rules = array();

    public static function make($rules, $inputs)
    {
        self::$rules = $rules;
        self::validator($inputs);
        return new static();
    }

    public static function validator($inputs)
    {
        foreach (self::$rules as $field => $rule) {

            $ruleNames = explode(",", $rule);

            foreach ($ruleNames as $ruleName) {
                if (strpos($ruleName, "-")) {
                    #for rules like min-4 or unique-{what table in DB?}users-{what column in DB?}username
                    self::multiValueRule($ruleName, $inputs, $field);
                } else {
                    self::$ruleName($inputs[$field], $field);
                }
            }
            self::addError($field, self::$fieldErrors);
        }

    }

    private static function multiValueRule($ruleName, $inputs, $field)
    {
        $ruleParams = explode("-", $ruleName);

        // [$functionName,$ruleParams]=explode("-", $ruleName);
        $functionName = $ruleParams[0];
        self::$functionName($inputs[$field], $field, $ruleParams);

        if (!function_exists($functionName)) {
            die("validation rule is not valid 2");
        }
    }

    private static function required($inputData, $fieldName)
    {
        if (empty($inputData)) {
            self::addFieldError("{$fieldName} is required");
            return false;
        }
        return true;
    }

    private static function email($inputData, $fieldName)
    {
        if (!filter_var($inputData, FILTER_VALIDATE_EMAIL)) {
            self::addFieldError("{$fieldName} is not a valid email");
            return false;
        }
        return true;
    }

    private static function date($inputData, $fieldName)
    {
        $dateArr = explode('-', $inputData);
        if (count($dateArr) == 3 && checkdate($dateArr[1], $dateArr[2], $dateArr[0])) {
            return true;
        }
        self::addFieldError("{$fieldName} is not a valid date");
        return false;
    }

    private static function upper($inputData, $fieldName)
    {
        if (!preg_match('@[A-Z]@', $inputData)) {
            self::addFieldError("{$fieldName} has not an upper case letter");
            return false;
        }
        return true;
    }

    private static function lower($inputData, $fieldName)
    {
        if (!preg_match('@[a-z]@', $inputData)) {
            self::addFieldError("{$fieldName} has not a lower case letter");
            return false;
        }
        return true;
    }

    private static function number($inputData, $fieldName)
    {
        if (!preg_match('@[0-9]@', $inputData)) {
            self::addFieldError("{$fieldName} should have numbers in it");
            return false;
        }
        return true;
    }

    private static function numeric($inputData, $fieldName)
    {
        if (!is_numeric($inputData)) {
            self::addFieldError("{$fieldName} should be a number");
            return false;
        }
        return true;
    }

    private static function alphaNumeric($inputData, $fieldName)
    {
        if (!ctype_alnum($inputData)) {
            self::addFieldError("{$fieldName} should be mix of letters and/or numbers");
            return false;
        }
        return true;
    }

    private static function min($inputData, $fieldName, $params)
    {
        if (strlen($inputData) < $params[1]) {
            self::addFieldError("{$fieldName} should be at least {$params[1]} characters long");
            return false;
        }
        return true;
    }

    /*private //todo
    function unique($inputData, $fieldName, $params)
    {
        $sql = new MysqlDb();
        $result = $sql->read($params[1], $params[2], "$params[2]='$inputData'");
        if (count($result) !== 0) {
            $this->addFieldError("{$fieldName} is not unique");
            return false;
        }
        return true;
    }*/

    private static function max($inputData, $fieldName, $params)
    {
        if (strlen($inputData) > $params[1]) {
            self::addFieldError("{$fieldName} can only be {$params[1]} characters long");
            return false;
        }
        return true;
    }

    public function failed()
    {
        if (self::hasError() === true) {
            return true;
        }
        return false;
    }
}