<?php


namespace Core\Config;


class DbConnection
{
    protected $connect;

    public function __construct()
    {
        $connection = mysqli_connect(HOST, USER, PASS, NAME);
        mysqli_set_charset($connection, "utf8");
        $this->connect = $connection;
    }

}