<?php

namespace Core;

use App\Models\User;
use Core\Traits\LogHandler;
use Core\Traits\Permission;

class Controller
{
    use LogHandler;
    use Permission;

    private $data = array();

    protected function view($page, $data = array())
    {
        $this->setData($data);
        extract($data);

        //if user has logged in store visited pages by users Id else store them with users Ip Address
        if (User::check()) {
            $this->collectUserLog(User::userId(), $page);
        } else {
            //put the logs in file\logs
            $this->collectUserLog($_SERVER["REMOTE_ADDR"], $page);
        }

        include(BASE_DIR . "App/Views/" . $page . ".php");
    }

    public function setData($data)
    {
        //merge all given data into 1 array
        $this->data = array_merge($this->data, $data);
    }

}