<?php
$file = $_SERVER["SCRIPT_FILENAME"];
$file = str_replace("Index.php", "", $file) . ".const";
$fileOpen = fopen($file, "r");
while (!feof($fileOpen)) {
    $eachLine = trim(fgets($fileOpen));
    $values = explode("=", $eachLine);
    if ($eachLine) {
        define($values[0], $values[1]);
    }
}