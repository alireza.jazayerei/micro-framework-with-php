<?php

namespace Core\Interfaces;

interface ModelContract
{

    public function create($table, $name, $value);

    public function update($table, $column, $value, $condition);

    public function delete($table, $condition);

    public function read($table, $column, $condition);
}