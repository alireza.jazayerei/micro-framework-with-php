<?php


namespace Core\Interfaces;


interface Middleware
{
    public function handler();
}