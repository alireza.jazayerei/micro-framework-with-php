<?php

namespace Core;


use Core\Config\DbConnection;
use Core\Interfaces\ModelContract;

class Model extends DbConnection implements ModelContract
{

    //queries are here we use them in models
    public function create($table, $name, $value)
    {
        if (is_array($name)) { //if we want to submit multiple name & values
            $value = implode("','", $value);
            $name = implode(",", $name);
        }
        $sql = "insert into $table ($name) values ('$value')";
        mysqli_query($this->connect, $sql);
    }

    public function update($table, $column, $values, $condition)
    {
        $text = array();
        foreach ($values as $key => $value) {
            $text[] = $column[$key] . "='" . $value . "'";
        }
        $query = implode(",", $text);
        $sql = "update $table set $query where $condition";
        mysqli_query($this->connect, $sql);
    }

    public function delete($table, $condition)
    {
        $sql = "delete from $table where $condition";
        mysqli_query($this->connect, $sql);
    }

    public function read($table, $column, $condition)
    {
        if (is_array($column)) {
            $column = implode(",", $column);
        }
        $sql = "SELECT $column FROM $table WHERE $condition";
        $status = mysqli_query($this->connect, $sql);
        $results = array();
        while ($result = mysqli_fetch_assoc($status)) {
            array_push($results, $result);
        }
        return $results;
    }

    public function __destruct()
    {
        mysqli_close($this->connect);
    }
}