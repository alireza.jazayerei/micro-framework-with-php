<?php

//define your routes here
$routes = [

    "" => [
        "type" => "get",
        "controller" => "Home",
        "method" => "index",
    ],
    "login" => [
        "type" => "get",
        "controller" => "User",
        "method" => "loginIndex",
    ],
    "login/dologin" => [
        "type" => "post",
        "controller" => "User",
        "method" => "doLogin",

    ],
    "dashboard" => [
        "type" => "get",
        "controller" => "Dashboard",
        "method" => "index",
        "middleware" => "auth",
    ],
    "logout" => [
        "type" => "get",
        "controller" => "user",
        "method" => "logOut",
        "middleware" => "auth",
    ],
    "signup" => [
        "type" => "get",
        "controller" => "User",
        "method" => "signUpIndex",

    ],
    "signup/dosignup" => [
        "type" => "post",
        "controller" => "User",
        "method" => "doSignUp",
    ],
    "404" => [ //A route for 404 error page
        "type" => "get",
        "controller" => "Error",
        "method" => "notFound",
    ],
    "dashboard/editusers" => [ //A route for 404 error page
        "type" => "get",
        "controller" => "User",
        "method" => "edit",
        "middleware" => "auth.permission-1",
    ],
    "dashboard/deleteusers" => [ //A route for 404 error page
        "type" => "get",
        "controller" => "User",
        "method" => "delete",
        "middleware" => "auth.permission-2",
    ],
];

